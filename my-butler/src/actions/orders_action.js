import {
    LIST_ORDERS_GET
} from './types';

export const getListOrders = () => {
    return (dispatch) => {
        return fetch('https://teeshinee.com/api/v1/orders', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Token c5115c4af4b9187ecfd03a151358aa6a5bc9d124'
                }
            })
            .then(res => res.json())
            .then((results) => {
                dispatch({
                    type: LIST_ORDERS_GET,
                    payload: {
                        props: results.results,
                        count: results.count
                    }
                })
            })
            .catch((error) => {
                console.error(error);
            });
    }
}