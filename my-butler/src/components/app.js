import React from 'react';
import Navigator from './navigations';
import Main from './main';
import Footer from './footer';
import Header from './header';
import {Container, Row, Col} from 'react-bootstrap';

export default () => (
    <Container>
        <Row className="header">
            <Header/>
        </Row>
        <Row style={{height: '90vh'}}>
            <Col xs={2} style={{backgroundColor: 'blue'}}>
                <Navigator />
            </Col>
            <Col xs={10}>
                <Main />
            </Col>
        </Row>
        <Row style={{height: '5vh'}}>
            <Footer />
        </Row>
    </Container>
)