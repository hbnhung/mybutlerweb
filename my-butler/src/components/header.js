import React from 'react';
import logo from '../asserts/img/logo.png';
import * as ReactBootstrap from 'react-bootstrap';

export default () => (
    <ReactBootstrap.Image className="logo" src={logo} rounded />
)