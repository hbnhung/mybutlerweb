import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Dashboard from './dashboard';
import Orders from '../containers/orders'

export default () => (
    <Switch>
        <Route exact path="/" component={Dashboard} />
        <Route exact path="/orders" component={Orders} />
    </Switch>
)