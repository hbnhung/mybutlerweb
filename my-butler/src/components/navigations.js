import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
    <div>
        <ul className="sidebar">
            <li><Link to="/">Trang chủ</Link></li>
            <li><Link to="/orders">Quản lý đơn hàng</Link></li>
            <li><Link to="/sampleHouse">Quản lý nhà mẫu</Link></li>
            <li><Link to="/about">Về chúng tôi</Link></li>
        </ul>
    </div>
)