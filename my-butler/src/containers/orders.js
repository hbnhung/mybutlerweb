import React, {Component} from 'react';
import {
    Table,
    Button
} from 'react-bootstrap';
import {
    connect
} from 'react-redux';
import {
    getListOrders
} from '../actions';

class Orders extends Component {
    constructor(props){
        super(props);
        this.state = {
            listItems: []
        }
    }


    componentWillMount() {
        this.props.getListOrders();
    }

    componentWillReceiveProps(nextProps){
        console.log('nextProps', nextProps);
        if(nextProps) {
           this.setState({listItems: nextProps.listItems});
        }
    }

    render (){       
        // loop order item
        for (let i=0; i< this.state.listItems.length; i++) {
            for (let j =0; j< this.state.listItems[i].order_items.length; j++) {
              //  this.state.listItems[i].order_items[j] = Object.entries(this.state.listItems[i].order_items[j]);
              const value = this.state.listItems[i].order_items[j];
              const orderItems = [];
              // orderItems.push(value.id);
              orderItems.push(<li key={value.id}>{value.service_name} - &nbsp;
                                SL:{value.quantity} -&nbsp; GT:{value.price}</li>);
              this.state.listItems[i].order_items[j] = orderItems;
            }
        }
        const listItems = this.state.listItems.map((value, index) => {
            return (
                <tr key={index}>
                    <td>*</td>
                    <td>{value.order_items}</td>
                    <td>{value.customer}</td>
                </tr>
            )
        });

        return (
            <div>
                <h3>Danh sách đơn hàng</h3>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Dịch vụ đăng ký</th>
                            <th>Địa chỉ</th>
                            <th>Dịch vụ</th>
                            <th>Thời gian bắt đầu</th>
                            <th>Lặp lại</th>
                            <th>Ghi chú</th>
                            <th>Tình trạng</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listItems}
                    </tbody>
                </Table>
            </div>
        );
    }
}

const mapStateToProps = ({ orders }) => {
    return {
        listItems: orders.arrOrders
    }
}

export default connect(mapStateToProps, {
    getListOrders
})(Orders);