import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter } from 'react-router-dom';
import App from './components/app';
import 'bootstrap/dist/css/bootstrap.min.css';
import thunk from 'redux-thunk';
import rootReducer from './reducers/index';
import "./style.css"

// const createStoreWithMiddleware = applyMiddleware()(createStore);
const store = createStore(
    rootReducer,
    applyMiddleware(thunk)
);
ReactDOM.render(
    //<Provider store={createStoreWithMiddleware(reducers)}>
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);