import { combineReducers } from 'redux';
import OrdersReducer from './reducer_orders';

export default combineReducers({
    orders : OrdersReducer
})
