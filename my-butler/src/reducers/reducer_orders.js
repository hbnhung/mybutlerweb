import {
    LIST_ORDERS_GET
} from '../actions/types';

const INITIAL_STATE = {
    arrOrders: [],
    count: ''
} 

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LIST_ORDERS_GET: {
            return {
                ...state,
                arrOrders: action.payload.props
            }
        }
        default: {
            return state;
        }
    }
}